<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<?php
	$product_id = get_the_ID();
	$product = new WC_product($product_id);
	$p_gallery_image = $product->get_gallery_image_ids();

	$p_madein = get_field('p_madein');
    $p_gallery = get_field('p_gallery');
	$p_overview = get_field('p_overview');
	$p_featured = get_field('p_featured');
	$p_skill = get_field('p_skill');
	$p_file = get_field('p_file');

	$p_info_brand = get_field('p_info_brand');
	$p_info_color = get_field('p_info_color');
	$p_info_thewoods = get_field('p_info_thewoods');
	$p_info_surface = get_field('p_info_surface');
	$p_info_style = get_field('p_info_style');
	$p_info_height = get_field('p_info_height');
	$p_info_width = get_field('p_info_width');
	$p_info_size = get_field('p_info_size');
	$p_info_packagesize = get_field('p_info_packagesize');
	$p_info_packageweight = get_field('p_info_packageweight');
	$p_info_border = get_field('p_info_border');
	$p_info_method = get_field('p_info_method');
	$p_info_guarantee = get_field('p_info_guarantee');
	$p_info_warranty = get_field('p_info_warranty');
	$p_info_productcode = get_field('p_info_productcode');
?>

<main id="main">
<div class="pageProductDetail">
	<div class="module module__pageProductDetail">
	    <div class="module module__header">
	      	<h2 class="title"><?php the_title(); ?></h2>
	    </div>

		<!-- <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>> -->
        <div id="product-<?php the_ID(); ?>">

            <div class="module__content">
                <div class="container">
                    <div class="product__detail">
                        <h3 class="product__title">
                            <span><?php _e('Tên sàn: ', 'text_domain'); ?></span>
                            <?php the_title(); ?>
                        </h3>
                        <div class="manufacturing">
                            <span><?php _e('Sản xuất: ', 'text_domain'); ?></span>
                            <?php echo $p_madein; ?>
                        </div>
                        <div class="product__detail-header">

                            <div class="product__item">
                                <div class="product__box">
                                    <div class="product__avata">

                                        <div class="product__slide slider-for">
                                        	<?php foreach( $p_gallery as $p_gallery_kq ){ ?>
                                            <div class="item">
                                                <img class="img__1" src="<?php echo $p_gallery_kq; ?>" />
                                                <!-- <img class="img__1" src="<?php echo wp_get_attachment_url( $p_gallery_image_kq ); ?>" /> -->
                                                <!-- <button class="btn btn__zoom" data-toggle="modal" data-target=".bd-example-modal-lg" data-img1="<?php echo wp_get_attachment_url( $p_gallery_image_kq ); ?>" data-title="<?php the_title(); ?>">
                                                    <img src="<?php echo asset('images/icons/icon__zoom.png'); ?>" alt="icon__zoom.png" />
                                                </button> -->
                                                <div class="control__zoom">
                                                    <button class="btn active btn__zoom2" data-toggle="modal" data-target=".bd-example-modal-lg" data-img1="" data-title="<?php the_title(); ?>">
                                                        <?php _e('Phóng to', 'text_domain'); ?>
                                                    </button>
                                                </div>
                                            </div>
                                        	<?php } ?>
                                        </div>

                                        <div class="product__slide slider-for btn__zoom3__slide">
                                            <?php foreach( $p_gallery_image as $p_gallery_image_kq ){ ?>
                                            <div class="item" style="display: none;">
                                                <div class="control__zoom">
                                                    <button class="btn active btn__zoom3" data-toggle="modal" data-target=".bd-example-modal-lg" data-img3="<?php echo wp_get_attachment_url( $p_gallery_image_kq ); ?>" data-title="<?php the_title(); ?>">
                                                        <?php _e('Phóng to', 'text_domain'); ?>
                                                    </button>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>

                                        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <button type="button" class="btn btn__close close" data-dismiss="modal" aria-label="Close">
                                                        <img class="colsImage" src="<?php echo asset('images/icons/icon__close.png'); ?>" alt="icon__close.png" />
                                                    </button>
                                                    <div id="module__detail"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

									<?php
										$money =  wc_get_product(get_the_ID());
										$price_old = (float)$money->get_regular_price();

										if(!empty($price_old)) { ?>

                                        <div class="product__control">
											<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
												<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt">
													<?php _e('Thêm vào giỏ', 'text_domain'); ?>
												</button>
											</form>
											<span class="btn product__price">
												<?php echo format_price($price_old).' '.get_woocommerce_currency_symbol(); ?>
											</span>
                                        </div>

									<?php } ?>


                                    <div class="productList">
                                        <div class="slider-nav">

                                        	<?php foreach( $p_gallery as $p_gallery_kq ){ ?>
                                            <div class="item">
                                                <div class="slider__box">
                                                    <div class="frame">
                                                        <img class="frame--image" src="<?php echo $p_gallery_kq; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        	<?php } ?>

                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="product__item">
                                <div class="box__info">
                                    <div class="bs-tab">
                                        <div class="tab-container">

                                            <div class="tab-control">
                                                <ul class="control-list">
                                                    <li class="control-list__item" tab-show="#tab1">
                                                        <?php _e('Tổng quan', 'text_domain'); ?>
                                                    </li>
                                                    <li class="control-list__item active" tab-show="#tab2">
                                                        <?php _e('Thông tin chi tiết sản phẩm', 'text_domain'); ?>
                                                    </li>
                                                    <li class="control-list__item" tab-show="#tab3">
                                                        <?php _e('Đặc trưng', 'text_domain'); ?>
                                                    </li>
                                                    <li class="control-list__item" tab-show="#tab4">
                                                        <?php _e('Kỹ thuật', 'text_domain'); ?>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tab-content">

                                                <div class="tab-item" id="tab1">
                                                    <div class="overview">

														<?php foreach ($p_overview as $p_overview_kq) { ?>
                                                            <h3><?php echo $p_overview_kq["title"]; ?></h3>
                                                            <p>
                                                                <?php echo $p_overview_kq["content"]; ?>
                                                            </p>
														<?php } ?>

                                                    </div>
                                                </div>
                                                <div class="tab-item active" id="tab2">
                                                    <div class="info__product">
                                                        <ul>
                                                            <li>
                                                                <span><?php _e('Nhãn hiệu ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_brand; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Màu sắc ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_color; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Các loại gỗ ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_thewoods; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Kết thúc bề mặt ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_surface; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Phong cách ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_style; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Độ dày ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_height; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Chiều rộng ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_width; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Kích thước ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_size; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Kích cỡ gói ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_packagesize; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Gói trọng lượng ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_packageweight; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Viền ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_border; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Phương pháp cài đặt ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_method; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Bảo hành trong nước ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_guarantee; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Bảo hành Thương mại ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_warranty; ?></span>
                                                            </li>
                                                            <li>
                                                            	<span><?php _e('Mã sản phẩm ', 'text_domain'); ?></span>
                                                                <span><?php echo $p_info_productcode; ?></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="tab-item" id="tab3">
                                                    <div class="overview">

														<?php foreach ($p_featured as $p_featured_kq) { ?>
                                                            <h3><?php echo $p_featured_kq["title"]; ?></h3>
                                                            <p>
                                                                <?php echo $p_featured_kq["content"]; ?>
                                                            </p>
														<?php } ?>

                                                    </div>
                                                </div>
                                                <div class="tab-item" id="tab4">
                                                    <div class="overview">

														<?php foreach ($p_skill as $p_skill_kq) { ?>
                                                            <h3><?php echo $p_skill_kq["title"]; ?></h3>
                                                            <p>
                                                                <?php echo $p_skill_kq["content"]; ?>
                                                            </p>
														<?php } ?>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                    <a class="btn btn__dowload" target="_blank" href="<?php echo $p_file; ?>">
                                    	<?php _e('Tải tài liệu', 'text_domain'); ?>
                                	</a>

                                </div>
                            </div>

						</div>
                        <div class="facebook">
                            <?php get_template_part("resources/views/social-bar"); ?>
                        </div>

                    </div>
                </div>
            </div>

		</div>

	</div>
</div>

	<!--related-->
	<?php do_action( 'woocommerce_after_single_product_summary' ); ?>
</main>

<?php do_action( 'woocommerce_after_single_product' ); ?>

<style type="text/css">
	.woocommerce-page div.product div.images, .woocommerce-page div.product div.summary {
	    float: none;
	    width: 100%;
	}
</style>

<script type="text/javascript">
    $('.product__avata .item .btn__zoom2').click(function() {

        var img_1 = $('.btn__zoom3__slide .slick-active .btn__zoom3').attr('data-img3');

        var title = $(this).attr('data-title');
        $("#module__detail").html(
            '<div class="modal__slide">' +
            '<div class="item">' +
            '<img class="img__1" src="' + img_1 + '" />' +
            ' </div>' +
            '</div>' +
            '<h2 class = "title" > ' + title + '</h2>');
        // console.log(img_1);
    });


	$('body').on("click", ".control-list__item", function () {
	    $(this).addClass("active");
	    $(this).siblings().removeClass("active");
	    $($(this).attr("tab-show")).slideDown(300);
	    $($(this).attr("tab-show")).siblings().slideUp(300);
	    $(this).parents(".tab-control").find(".control__show").html($(this).html());
	    $(this).parent(".control-list").removeClass("active");
	    $(this).parents(".tab-control").find(".control__show").removeClass("active");
	});
	$('body').on("click", ".control__show", function () {
	    $(".control-list").addClass("active");
	    $(this).addClass("active");
	});



</script>
