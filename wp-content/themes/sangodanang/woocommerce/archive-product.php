<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>


<?php
	$terms_info = get_terms( 'product_cat', array(
		'parent'=> 0,
	    'hide_empty' => false
	) );
?>

<main id="main">

	<section class="pageproduct">
	    <div class="container">
	        <div class="module pageproduct__module">
	            <div class="module__header">
	                <h2 class="title">
	                    <?php _e('Sản phẩm', 'text_domain'); ?>
	                </h2>
	            </div>
	        </div>
	    </div>
	</section>

	<section class="section__porduct">
	    <div class="container">
	        <div class="module module__porduct">

	            <div class="module__header">
	                <h2 class="title">
	                    <?php
	                    	// _e('Đồ gỗ', 'text_domain');
                    	?>
	                    THE WOOD
	                </h2>
	                <div class="control">
	                    <div class="bs-tab">
	                        <div class="tab-control">
	                            <ul class="control-list">

	                                <a href="javascript:void(0)" class="control-list__item active">
	                                	<?php _e('Tất cả', 'text_domain'); ?>
                                	</a>
									
									<?php foreach ($terms_info as $terms_info_kq) { ?>
										<a href="<?php echo get_term_link(get_term($terms_info_kq->term_id)); ?>" class="control-list__item">
											<?php echo $terms_info_kq->name; ?>
										</a>
									<?php } ?>

	                            </ul>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="module__content">
	                <div class="row">

						<?php
							$query = query_post_by_custompost_paged('product', 8);
							if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

			                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
								    <div class="product">
								        <div class="product__avata">
								            <a href="<?php the_permalink(); ?>" class="frame d-block">
								                <img class="frame--image" src="<?php echo getPostImage(get_the_ID(),"p-product"); ?>" alt="<?php the_title(); ?>">
								            </a>
								        </div>
								        <div class="product__content">
								            <div class="product__control">
								                <h3 class="product__title">
								                    <a href="<?php the_permalink(); ?>" class="product__link">
								                        <?php the_title(); ?>
								                    </a>
								                </h3>
								                <div class="time">
								                    <span class="icons"><i class="far fa-clock"></i></span>
								                    <span class="span__name"><?php echo get_the_date('d/m/Y');?></span>
								                </div>
								            </div>
								            <p class="product__desc">
								                <?php echo cut_string(get_the_excerpt(),100,'...'); ?>
								            </p>
								            <div class="product__footer">
								                <a href="<?php the_permalink(); ?>" class="btn btn__link">
								                    <?php _e('Chi tiết', 'text_domain'); ?>
								                </a>
								                <span class="product__price">
								                    <?php
														$money =  wc_get_product(get_the_ID());
														$price_old = (float)$money->get_regular_price();
														if(!empty($price_old)) {
															echo format_price($price_old).' '.get_woocommerce_currency_symbol();
														}
								                    ?>
								                </span>
								            </div>
								        </div>
								    </div>
								</div>

						<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                	</div>

					<?php
					if(wc_get_loop_prop( 'total_pages' ) > 1) {
						$total   = isset( $total ) ? $total : wc_get_loop_prop( 'total_pages' ); //tong so page
						$current = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' ); //page hien tai
						$base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ); //link
						$format  = isset( $format ) ? $format : '';

						if ( $total <= 1 ) {
							return;
						}
						?>
						<nav class="pagination">
							<?php
							echo paginate_links(
								apply_filters(
									'woocommerce_pagination_args',
									array( // WPCS: XSS ok.
										'base'      => $base,
										'format'    => $format,
										'add_args'  => false,
										'current'   => max( 1, $current ),
										'total'     => $total,
										'prev_text' => '«',
										'next_text' => '»',
										'type'      => 'list',
										'end_size'  => 3,
										'mid_size'  => 3,
									)
								)
							);
							?>
						</nav>
					<?php } ?>

	            </div>

	        </div>
	    </div>
	</section>

</main>

<?php get_footer( 'shop' ); ?>
