<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	global $post;
	$terms = get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
		'post_type' => 'product',
		'tax_query' => array(
			array(
				'taxonomy' => 'product_cat',
				'field' => 'id',
				'terms' => $term_ids,
				'operator'=> 'IN'
			 )),
		'posts_per_page' => 100,
		'orderby' => 'date',
		'post__not_in'=>array($post->ID)
	 ) );
?>

<div class="samekind">
  <div class="container">
    <div class="module module__samekind">
      <div class="module__header">
        <h2 class="title"><?php _e('Các sản phẩm liên quan', 'text_domain'); ?></h2>
      </div>
      <div class="module__content">
        <div class="samekind__slide">
          <div class="samekind__slick">

			<?php
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

		            <div class="item">
		              <div class="item__box">
		                <div class="product">
		                  <div class="product__avata">
		                    <a href="<?php the_permalink(); ?>" class="frame d-block">
		                      <img
		                        class="frame--image"
		                        src="<?php echo getPostImage(get_the_ID(),"p-product"); ?>"
		                        alt="<?php the_title(); ?>"
		                      />
		                    </a>
		                  </div>
		                  <div class="product__content">
		                    <div class="product__control">
		                      <h3 class="product__title">
		                        <a href="<?php the_permalink(); ?>" class="product__link">
		                          <?php the_title(); ?>
		                        </a>
		                      </h3>
		                      <div class="time">
		                        <span class="icons">
		                          <i class="far fa-clock"></i>
		                        </span>
		                        <span class="span__name"><?php echo get_the_date('d/m/Y');?></span>
		                      </div>
		                    </div>
		                    <p class="product__desc">
		                      <?php echo cut_string(get_the_excerpt(),100,'...'); ?>
		                    </p>
		                    <div class="product__footer">
		                      <a href="<?php the_permalink(); ?>" class="btn btn__link">
		                        <?php _e('Chi tiết', 'text_domain'); ?>
		                      </a>
		                      <span class="product__price">
				                    <?php
										$money =  wc_get_product(get_the_ID());
										$price_old = (float)$money->get_regular_price();
										if(!empty($price_old)) {
											echo format_price($price_old).' '.get_woocommerce_currency_symbol();
										}
				                    ?>
		                      </span>
		                    </div>
		                  </div>
		                </div>
		              </div>
		            </div>

			<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
