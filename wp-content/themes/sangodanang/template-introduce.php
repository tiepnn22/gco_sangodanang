<?php
/**
 * Template Name: Template Introduce
 * 
 */
?>

<?php get_header(); ?>

<?php
    $name_page = get_the_title();
    $introduce_info = get_field('introduce_info');
?>

<main id="main">
	
	<div class="module__tab module__tab_about_us">

		<div class="tab_header">
			<ul class="tab_about_us">

				<?php $i=1; foreach ($introduce_info as $introduce_info_kq) { ?>
					<li>
						<a href="javascript:void(0)" class="item_<?php echo $i;?>  <?php if($i==1){echo 'active';}?>">
							<?php echo $introduce_info_kq["title"]; ?>
						</a>
					</li>
				<?php $i++; } ?>

			</ul> 
		</div>

		<div class="tab_contents tab_about_us">

			<?php $i=1; foreach ($introduce_info as $introduce_info_kq) { ?>
				<div class="tab_content tab_content_<?php echo $i;?>  <?php if($i==1){echo 'active';}?>">
					<article class="section section-banner section-about-us">
						<div class="container">
							<div class="row">

								<?php if(!empty($introduce_info_kq["gallery"])) { ?>
								<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
									<div class="module module__about_us_galary">
										<div id="dg-container-<?php echo $i; ?>" class="dg-container">
											<div class="dg-wrapper">
												<?php foreach ($introduce_info_kq["gallery"] as $introduce_info_gallery) { ?>
													<a href="javascript:void(0)">
														<img src="<?php echo $introduce_info_gallery;?>">
													</a>
												<?php } ?>
											</div>
											<nav>	
												<span class="dg-icon dg-prev">
													<i class="fal fa-chevron-left icon"></i>
												</span>
												<span class="dg-icon dg-next">
													<i class="fal fa-chevron-right icon"></i>
												</span>
											</nav>
										</div>
									</div>
								</div>
								<?php } ?>

								<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
									<div class="module module__about_us">


                                        <div class="module__header">
                                            <h2 class="title"><?php echo $introduce_info_kq["desc_title"]; ?> <span><?php echo $introduce_info_kq["desc_title_green"]; ?></span></h2>
                                            <p class="info"><?php echo $introduce_info_kq["desc_title_meta"]; ?></p>
                                        </div>
                                        <div class="module__content">
                                            <div class="content">
                                                <?php echo wpautop( $introduce_info_kq["desc"] ); ?>
                                            </div>
                                        </div>


									</div>
								</div>

								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
									<div class="module module__about_us_content">
										<div class="content">
								        	<?php echo wpautop( $introduce_info_kq["content"] ); ?>
								        </div>
									</div>
								</div>

							</div>
						</div>
					</article>
				</div>
			<?php $i++; } ?>

		</div>

	</div>

</main>

<?php get_footer(); ?>
