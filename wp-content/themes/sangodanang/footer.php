<footer id="footer">
    <div class="container">

        <div class="footer__group">
            <div class="footer__item">
                <div class="logo">
                    <a href="<?php echo get_option('home');?>" class="logo__link">
                        <img src="<?php echo get_theme_mod('footer_logo');?>" alt="<?php echo get_option('blogname'); ?>">
                    </a>
                </div>
                <div class="item__text">
                    
                    <?php echo get_data_language(wpautop( get_theme_mod('footer_info') ), wpautop( get_theme_mod('footer_info_en') )); ?>
                </div>
            </div>
            <div class="footer__item">
                <?php
                    if(!empty(get_theme_mod('footer_form'))) {$footer_form = do_shortcode(get_theme_mod('footer_form'));}
                    if(!empty(get_theme_mod('footer_form_en'))) {$footer_form_en = do_shortcode(get_theme_mod('footer_form_en'));}
                    echo get_data_language( $footer_form, $footer_form_en);
                ?>
                <div class="footer__society">
                    <a href="<?php echo get_theme_mod( 'header_gmail' );?>" class="society__item">
                        <img src="<?php echo asset('images/icons/icon__google.png'); ?>">
                    </a>
                    <a href="<?php echo get_theme_mod( 'header_facebook' );?>" class="society__item">
                        <img src="<?php echo asset('images/icons/icon__facebook.png'); ?>">
                    </a>
                </div>
            </div>
        </div>

        <?php
            if(function_exists('wp_nav_menu')){
                $args = array(
                    'theme_location' => 'menu_footer',
                    'container_class'=>'container_class',
                    'menu_class'=>'footer__menu',
                );
                wp_nav_menu( $args );
            }
        ?>

    </div>
</footer>


<button class="back-top">
    <i class="fas fa-arrow-up"></i>
</button>
<div id="tool__society">
    <div class="tool__item">
        <a href="tel:<?php echo str_replace(' ','',get_field('global_phone', 'option'));?>" class="tool__icon">
            <img src="<?php echo asset('images/icons/icon__mobile.png'); ?>">
        </a>
        <a href="https://zalo.me/<?php echo str_replace(' ','',get_field('global_zalo', 'option'));?>" class="tool__icon">
           <img src="<?php echo asset('images/icons/icon__zalo.png'); ?>"> 
        </a>
        <a href="#" class="tool__icon">
            <img src="<?php echo asset('images/icons/icon__message.png'); ?>">
        </a>
    </div>
</div>


<?php wp_footer(); ?>
</body>
</html>