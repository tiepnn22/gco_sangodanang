<?php
/**
 * Template Name: Template Contact
 * 
 */
?>

<?php get_header(); ?>

<?php
    $name_page = get_the_title();
    $contact_map = get_field('contact_map');
    if(!empty(get_theme_mod('footer_form'))) {$footer_form = do_shortcode(get_theme_mod('footer_form'));}
    if(!empty(get_theme_mod('footer_form_en'))) {$footer_form_en = do_shortcode(get_theme_mod('footer_form_en'));}
?>

<main id="main">

    <article class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="module module__breadcrumbs">
                        <div class="module__header">
                            <h1 class="title"><?php echo $name_page; ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <article class="section section-banner section-contacts">
        <div class="container">
            <div class="row">

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="module module__address">
                        <?php echo wpautop( the_content() ); ?>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="module module__contacts">
                        <?php
                            echo get_data_language( $footer_form, $footer_form_en);
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </article>

    <article class="section section-maps">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="module module__maps">
                        <div class="module__content">
                            <div class="content">
                                <?php echo $contact_map; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

</main>


<?php get_footer(); ?>
