<?php
/**
 * Template Name: Template Service
 * 
 */
?>

<?php get_header(); ?>

<?php
    $name_page = get_the_title();
    $service_cat = get_field('service_cat');
?>

<main id="main">
        
    <article class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="module module__breadcrumbs">
                        <div class="module__header">
                            <h1 class="title"><?php echo $name_page; ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <article class="section section-services">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="module module__services module__service__detail">
                        <div class="module__content">

                            <div class="groups_box">
                                <?php
                                    $query = query_post_by_category_paged($service_cat, 6);
                                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

                                        <div class="item">
                                            <div class="module_item service_item">
                                                <div class="item_image">
                                                    <div class="image">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <img src="<?php echo getPostImage(get_the_ID(),"p-product"); ?>" alt="<?php the_title(); ?>">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="item_contents">
                                                    <h4 class="item_name">
                                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>

                                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>
                            </div>

                            <nav class="navigation">
                                <?php wp_pagenavi( array( 'query' => $query ) ); ?>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

</main>

<?php get_footer(); ?>
