<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body id="<?php echo 'language-'.get_data_language('vi', 'en'); ?>" <?php body_class(); ?>>
    

<header id="header">
    <div class="container">
        <div class="header__group">

            <div class="group__item">
                <h1 class="logo">
                    <!-- <a href="<?php echo get_option('home');?>" class="logo__link"> -->
                    <a href="<?php echo get_data_language(get_option('home'), get_option('home').'/en');?>" class="logo__link">
                        <img src="<?php echo get_theme_mod('header_logo');?>" alt="<?php echo get_option('blogname'); ?>">
                    </a>
                </h1>
            </div>

            <div class="group__item">
                <div class="header__top">

                    <div class="top__item">
                        <p class="hotline">
                            <?php _e('Hotline: ', 'text_domain'); ?>
                            <a href="tel:<?php echo str_replace(' ','',get_theme_mod( 'header_phone' ));?>">
                                <?php echo get_theme_mod( 'header_phone' );?>
                            </a>
                        </p>
                    </div>

                    <div class="top__item">

                        <a href="<?php echo get_data_language(get_page_link(34), get_page_link(11));?>" class="cart">
                            <span class="cart__number">
                                <?php echo WC()->cart->get_cart_contents_count(); ?>
                            </span>
                            <div class="cart__icon">
                                <img class="icon__image" src="<?php echo asset('images/icons/icon__card.png'); ?>">
                                <img class="icon__image_hover" src="<?php echo asset('images/icons/icon__card__hover.png'); ?>">
                            </div>
                            <span class="cart__name">
                                <?php _e('Giỏ hàng', 'text_domain'); ?>
                            </span>
                        </a>

                        <div class="search">
                            <form class="form" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
                                <input type="text" class="form-control input__search" placeholder="<?php _e('Tìm kiếm...', 'text_domain'); ?>" id="search-box" name="s" value="<?php echo get_search_query(); ?>">
                                <button class="btn btn__search">
                                    <i class="fas fa-search"></i>
                                </button>
                            </form>
                        </div>

                        <div class="language">
                            <?php pll_the_languages(array('show_names'=>1)); ?>
                        </div>

                        <div class="society">
                            <a href="<?php echo get_theme_mod( 'header_gmail' );?>" class="society__link">
                                <img src="<?php echo asset('images/icons/icon__google.png'); ?>">
                            </a>
                            <a href="<?php echo get_theme_mod( 'header_facebook' );?>" class="society__link">
                                <img src="<?php echo asset('images/icons/icon__facebook.png'); ?>">
                            </a>
                        </div>

                        <button class="btn btn__back">
                            <i class="fas fa-arrow-left"></i>
                        </button>
                        <button class="btn btn__menu">
                            <i class="fas fa-bars"></i>
                        </button>

                    </div>
                </div>

                <div class="menu__container">
                    <div class=" header__menu">

                        <?php
                            if(function_exists('wp_nav_menu')){
                                $args = array(
                                    'theme_location' => 'primary',
                                    'container_class'=>'container_class',
                                    'menu_class'=>'menu',
                                );
                                wp_nav_menu( $args );
                            }
                        ?>

                        <div class="mobile">
                            <p class="hotline">
                                <?php _e('Hotline: ', 'text_domain'); ?>
                                <a href="tel:<?php echo str_replace(' ','',get_theme_mod( 'header_phone' ));?>">
                                    <?php echo get_theme_mod( 'header_phone' );?>
                                </a>
                            </p>
                            <div class="society">
                                <a href="#" class="society__link">
                                    <img src="<?php echo asset('images/icons/icon__google.png'); ?>">
                                </a>
                                <a href="#" class="society__link">
                                    <img src="<?php echo asset('images/icons/icon__facebook.png'); ?>">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</header>


<?php
    $banner = get_field('banner', 'option');
?>

<section class="slide__banner">
    <div class="slick__banner">

        <?php foreach ($banner as $banner_kq) { ?>
            <div class="slide__item">
                <div class="frame">
                    <img class="frame--image" data-lazy="<?php echo $banner_kq; ?>">
                </div>
            </div>
        <?php } ?>

    </div>
</section>

