<?php get_header(); ?>

<?php
	$home_intro_gallery = get_field('home_intro_gallery');
	$home_intro_title = get_field('home_intro_title');
	$home_intro_title_green = get_field('home_intro_title_green');
	$home_intro_title_meta = get_field('home_intro_title_meta');
	$home_intro_desc = get_field('home_intro_desc');
	$home_intro_link = get_field('home_intro_link');

	$home_service_title = get_field('home_service_title');
	$home_service = get_field('home_service');

	$home_ads = get_field('home_ads');

	$home_testimonial_title = get_field('home_testimonial_title');
	$home_testimonial = get_field('home_testimonial');

	$home_news_title = get_field('home_news_title');
	$home_news = get_field('home_news');

	$terms_info = get_terms( 'product_cat', array(
		'parent'=> 0,
	    'hide_empty' => false
	) );
?>

<main id="main" class="">

	<article class="section section-banner section-about-us">
	    <div class="container">
	        <div class="row">
	        	<?php if(!empty($home_intro_gallery)) { ?>
	            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
	                <div class="module module__about_us_galary">
	                    <div id="dg-container" class="dg-container">
	                        <div class="dg-wrapper">
								<?php foreach ($home_intro_gallery as $home_intro_gallery_kq) { ?>
									<a href="javascript:void(0)">
										<img src="<?php echo $home_intro_gallery_kq;?>">
									</a>
								<?php } ?>
	                        </div>
	                        <nav>
	                            <span class="dg-icon dg-prev"><i class="fal fa-chevron-left icon"></i></span>
	                            <span class="dg-icon dg-next"><i class="fal fa-chevron-right icon"></i></span>
	                        </nav>
	                    </div>
	                </div>
	            </div>
	            <?php } ?>
	            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
	                <div class="module module__about_us">
	                    <div class="module__header">
	                        <h2 class="title"><?php echo $home_intro_title; ?> <span><?php echo $home_intro_title_green; ?></span></h2>
	                        <p class="info"><?php echo $home_intro_title_meta; ?></p>
	                    </div>
	                    <div class="module__content">
	                        <div class="content">
	                            <?php echo wpautop( $home_intro_desc ); ?>
	                        </div>
	                        <div class="button">
	                            <a href="<?php echo $home_intro_link; ?>" class="btn btn__view">
	                            	<?php _e('Tìm hiểu thêm', 'text_domain'); ?>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>
	
	<?php if(!empty($home_service)) { ?>
	<article class="section section-services">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__services">
	                    <div class="module__header">
	                        <h2 class="title"><span><?php echo $home_service_title; ?></span></h2>
	                    </div>
	                    <div class="module__content">
	                        <!-- <div class="groups_box"> -->
                        	<div class="slick_slide slick_services">

                                <?php
                                    $query = query_post_by_category($home_service, 100);
                                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

                                        <div class="item">
                                            <div class="module_item service_item">
                                                <div class="item_image">
                                                    <div class="image">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <img src="<?php echo getPostImage(get_the_ID(),"p-product"); ?>" alt="<?php the_title(); ?>">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="item_contents">
                                                    <h4 class="item_name">
                                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                    </h4>
													<a href="<?php the_permalink(); ?>" class="btn btn__view btn--green-line">
	                                            		<?php _e('Chi tiết', 'text_domain'); ?>
	                                        		</a>
                                                </div>
                                            </div>
                                        </div>

                                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>
	<?php } ?>

	<?php if(!empty($home_ads)) { ?>
	<article class="section section-features">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__features">
	                    <div class="module__content">
	                        <div class="groups_box">

								<?php foreach ($home_ads as $home_ads_kq) { ?>
		                            <div class="item">
		                                <div class="module_item feature_item">
		                                    <div class="item_image">
		                                        <div class="image">
		                                            <img src="<?php echo $home_ads_kq['image']; ?>" alt="<?php echo $home_ads_kq['title']; ?>">
		                                        </div>
		                                    </div>
		                                    <div class="item_contents">
		                                        <h4 class="item_name">
		                                            <span><?php echo $home_ads_kq['title']; ?></span>
		                                            <span><?php echo $home_ads_kq['desc']; ?></span>
		                                        </h4>
		                                    </div>
		                                </div>
		                            </div>
								<?php } ?>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>
	<?php } ?>

	<article class="section section-products">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__products">
	                    <div class="module__content">
	                        <div class="module__tab">

	                            <div class="tab_header">
	                                <div class="module__header">
	                                    <h2 class="title line_left"><span><?php _e('Đồ gỗ', 'text_domain'); ?></span></h2>
	                                </div>
	                                <ul class="tab_products">
	                                    <li>
	                                        <a href="javascript:void(0)" title="Tất cả" class="item_1 active">
	                                        	<?php _e('Tất cả', 'text_domain'); ?>
                                        	</a>
	                                    </li>

										<?php $i=2; foreach ($terms_info as $terms_info_kq) { ?>
											<li>
												<a href="javascript:void(0)" class="item_<?php echo $i;?>">
													<?php echo $terms_info_kq->name; ?>
												</a>
											</li>
										<?php $i++; } ?>

	                                </ul>
	                            </div>

	                            <div class="tab_contents tab_products">

	                                <div class="tab_content tab_content_1 active">
	                                    <div class="groups_box">

											<?php
												$query = query_post_by_custompost('product', 8);
												if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

													<div class="item">
													    <div class="module_item product_item">
													        <div class="item_images">
													            <div class="image">
													                <a href="<?php the_permalink(); ?>">
													                    <img src="<?php echo getPostImage(get_the_ID(),"p-product"); ?>" alt="<?php the_title(); ?>">
													                </a>
													            </div>
													        </div>
													        <div class="item_contents">
													            <h4 class="item_name">
													                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													                <span class="item_create">
													                    <i class="fal fa-clock icon"></i> <?php echo get_the_date('d/m/Y');?>
													                </span>
													            </h4>
													            <div class="item_content">
													                <?php echo cut_string(get_the_excerpt(),100,'...'); ?>
													            </div>
													            <div class="item_btn">
													                <a href="<?php the_permalink(); ?>" class="btn btn_2">
													                	<?php _e('Chi tiết', 'text_domain'); ?>
													            	</a>
													                <span class="item_price">
													                    <?php
																			$money =  wc_get_product(get_the_ID());
																			$price_old = (float)$money->get_regular_price();
																			if(!empty($price_old)) {
																				echo format_price($price_old).' '.get_woocommerce_currency_symbol();
																			}
													                    ?>
													                </span>
													            </div>
													        </div>
													    </div>
													</div>

											<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

	                                    </div>
	                                </div>

									<?php $i=2; foreach ($terms_info as $terms_info_kq) { ?>
		                                <div class="tab_content tab_content_<?php echo $i;?>">
		                                    <div class="groups_box">

												<?php
													$query = query_post_by_taxonomy('product', 'product_cat', $terms_info_kq, 8);
													if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

														<div class="item">
														    <div class="module_item product_item">
														        <div class="item_images">
														            <div class="image">
														                <a href="<?php the_permalink(); ?>">
														                    <img src="<?php echo getPostImage(get_the_ID(),"p-product"); ?>" alt="<?php the_title(); ?>">
														                </a>
														            </div>
														        </div>
														        <div class="item_contents">
														            <h4 class="item_name">
														                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														                <span class="item_create">
														                    <i class="fal fa-clock icon"></i> <?php echo get_the_date('d/m/Y');?>
														                </span>
														            </h4>
														            <div class="item_content">
														                <?php echo cut_string(get_the_excerpt(),100,'...'); ?>
														            </div>
														            <div class="item_btn">
														                <a href="<?php the_permalink(); ?>" class="btn btn_2">
														                	<?php _e('Chi tiết', 'text_domain'); ?>
														            	</a>
														                <span class="item_price">
														                    <?php
																				$money =  wc_get_product(get_the_ID());
																				$price_old = (float)$money->get_regular_price();
																				if(!empty($price_old)) {
																					echo format_price($price_old).' '.get_woocommerce_currency_symbol();
																				}
														                    ?>
														                </span>
														            </div>
														        </div>
														    </div>
														</div>

												<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

		                                    </div>
		                                </div>
									<?php $i++; } ?>

	                            </div>

	                            <div class="button">
	                                <a href="<?php echo get_data_language(get_page_link(331), get_page_link(10));?>" class="btn btn__view">
	                                	<?php _e('Xem thêm', 'text_domain'); ?>
                                	</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>

	<?php if(!empty($home_testimonial)) { ?>
	<article class="section section-testimonials">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__testimonials">
	                    <div class="module__header">
	                        <h2 class="title"><?php echo $home_testimonial_title; ?></h2>
	                    </div>
	                    <div class="module__content">
	                        <div class="slick_slide slick_testimonials">

								<?php foreach ($home_testimonial as $home_testimonial_kq) { ?>
									<div class="item">
		                                <div class="module_item testimonial_item">
		                                    <div class="item_images">
		                                        <div class="image">
		                                            <img src="<?php echo $home_testimonial_kq['image']; ?>" alt="<?php echo $home_testimonial_kq['name']; ?>">
		                                        </div>
		                                    </div>
		                                    <div class="item_contents">
		                                        <h4 class="item_name">
		                                            <span><?php echo $home_testimonial_kq['title']; ?></span>
		                                            <span><?php echo $home_testimonial_kq['job']; ?></span>
		                                        </h4>
		                                        <div class="item_content">
		                                            <?php echo $home_testimonial_kq['desc']; ?>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
								<?php } ?>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>
	<?php } ?>

	<?php if(!empty($home_news)) { ?>
	<article class="section section-blogs">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__blogs">
	                    <div class="module__header">
	                        <h2 class="title">
	                            <span><?php echo $home_news_title; ?></span>
	                        </h2>
	                    </div>
	                    <div class="module__content">
	                        <div class="slick_slide slick_blogs">

                                <?php
                                    $query = query_post_by_category($home_news, 100);
                                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

		                                <div class="item">
		                                    <div class="module_item blog_item">
		                                        <div class="item_contents">
		                                            <h4 class="item_name">
		                                                <a href="<?php the_permalink(); ?>">
		                                                    <?php the_title(); ?>
		                                                </a>
		                                            </h4>
		                                            <div class="item_create">
														<label><?php echo get_the_date('d'); ?></label>
									                    <span><?php _e('Tháng', 'text_domain'); ?> <?php echo get_the_date('m/Y');?></span>
		                                            </div>
		                                        </div>
		                                        <div class="item_images">
		                                            <div class="image">
		                                                <a href="<?php the_permalink(); ?>">
		                                                    <img src="<?php echo getPostImage(get_the_ID(),"p-news"); ?>" alt="<?php the_title(); ?>">
		                                                </a>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>

                                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                            </div>
                            <div class="button">
                                <a href="<?php echo get_term_link(get_term($home_news)); ?>" class="btn btn__view">
                                	<?php _e('Xem thêm', 'text_domain'); ?>
                            	</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</article>
	<?php } ?>

</main>


<?php get_footer(); ?>

