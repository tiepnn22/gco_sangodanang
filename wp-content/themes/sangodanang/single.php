<?php get_header(); ?>

<?php
	$post_id = get_the_ID();
	$author_id = get_post_field( 'post_author', $post_id );
	$author_name = get_the_author_meta('user_nicename', $author_id);
?>

<main id="main">

	<article class="section section-blogs">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__blogs module__blog__detail">
	                    <div class="module__header">
	                        <h1 class="title"><?php the_title(); ?></h1>
	                        <div class="item_admin">
	                            <i class="fal fa-user icon"></i><?php echo $author_name; ?>
	                        </div>
	                        <div class="item_create">
	                            <i class="fal fa-clock icon"></i><?php echo get_the_date('d-m-Y');?>
	                        </div>
	                    </div>
	                    <div class="module__content">
	                        <div class="content"><?php echo wpautop( the_content() ); ?></div>
	                    </div>

	                    <div class="art-comments">
							<?php get_template_part("resources/views/social-bar"); ?>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>

	<?php get_template_part("resources/views/related-post"); ?>

</main>

<?php get_footer(); ?>