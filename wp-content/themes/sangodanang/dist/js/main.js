$(document).ready(function() {
    function menuMobile() {
        var hasMenu = $('.header__menu .menu .menu__item:has(".omega__menu")');
        if (hasMenu) {
            hasMenu.append('<button class="btn toogleMenu"><i class="fal fa-angle-down"></i></button>');
            $('.toogleMenu').click(function() {
                $('.omega__menu').slideToggle();
                $(this).children('i').toggleClass('active');

            });
            $('.btn__menu').click(function() {
                $('body').toggleClass('open__fixed');
                $('.header__group').toggleClass('active');
            });
            $('.btn__back').click(function() {
                $('.header__group').removeClass('active');
                $('body').removeClass('open__fixed');

            })
        }
    }
    menuMobile();


    $('#header .header__group .group__item .menu__container .header__menu .menu li').click(function(){
        $(this).find('.sub-menu').stop(true,false).slideToggle();
    });

    $('#header .header__group .group__item .menu__container .header__menu .menu li').find('.sub-menu').parent().addClass('tamgiac');

    
    // function detailZoom() {
    //     $('.btn__zome').click(function() {
    //         $('.btn__zome').removeClass('active');
    //         $(this).addClass('active');

    //     });
    //     $('.btn__zoom').click(function() {

    //         var img_1 = $(this).attr('data-img1');
    //         var img_2 = $(this).attr('data-img2');

    //         var title = $(this).attr('data-title');
    //         $("#module__detail").html(
    //             '<div class="modal__slide">' +
    //             '<div class="item">' +
    //             '<img class="img__2" src="' + img_2 + '" />' +
    //             '<img class="img__1" src="' + img_1 + '" />' +
    //             ' </div>' +
    //             '</div>' +
    //             '<h2 class = "title" > ' + title + '</h2>');
    //     });
    //     $('.roomView').click(function() {
    //         $(this).parents('.item').children('.img__1').css('display', 'block');
    //         $(this).parents('.item').children('.img__2').css('display', 'none');
    //         $('#module__detail').removeClass('active');


    //     });
    //     $('.closeUP').click(function() {
    //         $(this).parents('.item').children('.img__1').css('display', 'none');
    //         $(this).parents('.item').children('.img__2').css('display', 'block');
    //         $('#module__detail').addClass('active');

    //     });
    // }
    // detailZoom();

    function slideBanner() {
        $('.slick__banner').slick({
            lazyLoad: 'ondemand',
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            // autoplay: true,
            arrows: false,

        })
    }
    slideBanner();

    function productDeatail() {

        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',

        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            focusOnSelect: true,
            nextArrow: "<button type='button' class='btn next__arrow'><i class='fal fa-chevron-right'></i></button>",
            prevArrow: "<button type='button' class='btn prev__arrow'><i class='fal fa-chevron-left'></i></button>",
        });

    }
    productDeatail();

    function slideSameKing() {
        $('.samekind__slick').slick({
            autoplay: true,
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            nextArrow: "<button type='button' class='btn next__arrow'><i class='fal fa-chevron-right'></i></button>",
            prevArrow: "<button type='button' class='btn prev__arrow'><i class='fal fa-chevron-left'></i></button>",
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]
        });
    }
    slideSameKing();
    $(function() {
        $('#dg-container').gallery();
    });

    $(".slick_testimonials").slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: '<a class="slick-arrow slick-prev" href="javascript:0"><i class="fal fa-chevron-left icon"></i></a>',
        nextArrow: '<a class="slick-arrow slick-next" href="javascript:0"><i class="fal fa-chevron-right icon"></i></a>',
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(".slick_blogs").slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: '<a class="slick-arrow slick-prev" href="javascript:0"><i class="fal fa-chevron-left icon"></i></a>',
        nextArrow: '<a class="slick-arrow slick-next" href="javascript:0"><i class="fal fa-chevron-right icon"></i></a>',
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(".slick_blogs_relate").slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: '<a class="slick-arrow slick-prev" href="javascript:0"><i class="fal fa-chevron-left icon"></i></a>',
        nextArrow: '<a class="slick-arrow slick-next" href="javascript:0"><i class="fal fa-chevron-right icon"></i></a>',
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(function() {
        $('#dg-container-1').gallery();
    });
    $(function() {
        $('#dg-container-2').gallery();
    });
    $(function() {
        $('#dg-container-3').gallery();
    });

    $(".slick_services").slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: '<a class="slick-arrow slick-prev" href="javascript:0"><i class="fal fa-chevron-left icon"></i></a>',
        nextArrow: '<a class="slick-arrow slick-next" href="javascript:0"><i class="fal fa-chevron-right icon"></i></a>',
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(window).on("load", function() {
        if ($(this).scrollTop() >= 42) {
            $("#header").addClass("scrolled");
        } else {
            $("#header").removeClass("scrolled");

        }
    });
    $(document).scroll(function() {
        if ($(this).scrollTop() >= 42) {
            $("#header").addClass("scrolled");
            $(".back-top").addClass("active");

        } else {
            $("#header").removeClass("scrolled");
            $(".back-top").removeClass("active");

        }
        if ($(this).scrollTop() >= 42) {
            $("#header").addClass("scrolled");
            $(".back-top").addClass("active");



        } else {
            $("#header").removeClass("scrolled");
            $(".back-top").removeClass("active");


        }
    });
    $(".back-top").on("click", function() {
        $(".back-top").removeClass("active");
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    });
});
jQuery(document).ready(function($) {
    $('.tab_header a').click(function(e) {
        e.preventDefault();

        var ac = $(this).hasClass('active');

        if (!ac) {
            var clnm = $(this).attr('class').replace('item_', '');
            var clsnm = '.tab_content_' + clnm;

            var natab = $(this).parent().parent().attr('class');

            $('.' + natab + ' > li > a').removeClass('active');
            $(this).addClass('active');
            $('.' + natab + ' > .tab_content').removeClass('active');
            $('.' + natab + ' > ' + clsnm).addClass('active');
        }
    });
});