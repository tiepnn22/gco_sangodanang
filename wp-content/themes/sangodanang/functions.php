<?php
include_once get_template_directory(). '/load/Custom_Functions.php';
include_once get_template_directory(). '/load/Customize.php';
// include_once get_template_directory(). '/load/CustomPost_CustomTaxonomy.php';
include_once get_template_directory(). '/load/wc.php';


// create_post_type("Services","service","service",array( 'title','editor','thumbnail','excerpt','comments'));
// create_taxonomy_theme("Categories Service","service_cat","service_cat","service");


//custom
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name;
        }

        if (is_404()) {
            return __( '404 page not found', 'text_domain' );
        }

        return get_the_title();
    }
}


if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}


if (!function_exists('getPostImage')) {
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image.png') : $img[0];
    }
}


if (!function_exists('cut_string')) {
    function cut_string($str,$len,$more){
        if ($str=="" || $str==NULL) return $str;
        if (is_array($str)) return $str;
            $str = trim(strip_tags($str));
        if (strlen($str) <= $len) return $str;
            $str = substr($str,0,$len);
        if ($str != "") {
            if (!substr_count($str," ")) {
              if ($more) $str .= " ...";
              return $str;
            }
            while(strlen($str) && ($str[strlen($str)-1] != " ")) {
                $str = substr($str,0,-1);
            }
            $str = substr($str,0,-1);
            if ($more) $str .= " ...";
        }
        return $str;
    }
}


// if (!function_exists('getPostViews')) {
//     function getPostViews($postID){
//         $count_key = 'post_views_count';
//         $count = get_post_meta($postID, $count_key, true);
//         if($count==''){
//             delete_post_meta($postID, $count_key);
//             add_post_meta($postID, $count_key, '0');
//             return "0";
//         }
//         return $count;
//     }
// }
// if (!function_exists('setPostViews')) {
//     function setPostViews($postID) {
//         $count_key = 'post_views_count';
//         $count = get_post_meta($postID, $count_key, true);
//         if($count==''){
//             $count = 0;
//             delete_post_meta($postID, $count_key);
//             add_post_meta($postID, $count_key, '0');
//         }else{
//             $count++;
//             update_post_meta($postID, $count_key, $count);
//         }
//     }
// }


if (!function_exists('format_price')) {
    function format_price($money)
    {
        $str = "";
        if ($money != 0) {
            $num = (float) $money;
            $str = number_format($num, 0, '.', '.');
        }
        return $str;
    }
}


if (!function_exists('get_data_language')) {
    function get_data_language($dataVN, $dataEN) {
        if(ICL_LANGUAGE_CODE == 'vi'){
            $get_data_language = $dataVN;
        } elseif (ICL_LANGUAGE_CODE == 'en') {
            $get_data_language = $dataEN;
        }
        return $get_data_language;
    }
}


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Theme options', // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Theme options', // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
}



