<?php
    global $post;
    $categories = get_the_category($post->ID);

    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
        'category__in' => $category_ids,
        'post__not_in' => array($post->ID),
        'posts_per_page'=> 4,
        'ignore_sticky_posts'=>1
    );
    $query = new wp_query( $args );
?>

<article class="section section-products">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="module module__products module__blogs__relate">
                    <div class="module__header">
                        <h2 class="title"><?php _e('Các bài viết liên quan', 'text_domain'); ?></h2>
                    </div>
                    <div class="module__content">
                        <div class="slick_slide slick_blogs_relate">

                            <?php if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

                                <div class="item">
                                    <div class="module_item product_item">
                                        <div class="item_images">
                                            <div class="image">
                                                <a href="<?php the_permalink()?>">
                                                    <img src="<?php echo getPostImage(get_the_ID(),"p-news"); ?>" alt="<?php the_title(); ?>">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item_contents">
                                            <h4 class="item_name">
                                                <a href="<?php the_permalink()?>"><?php the_title(); ?></a>
                                                <span class="item_create">
                                                    <i class="fal fa-clock icon"></i> <?php echo get_the_date('d-m-Y');?>
                                                </span>
                                            </h4>
                                            <div class="item_content">
                                                <p><?php echo cut_string(get_the_excerpt(),100,'...'); ?></p>
                                            </div>
                                            <div class="item_btn">
                                                <a href="<?php the_permalink()?>" class="btn btn_2">Chi tiết</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                    		<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>