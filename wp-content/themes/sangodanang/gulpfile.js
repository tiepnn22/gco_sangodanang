var gulp = require("gulp");
var sass = require("gulp-sass");
var cssnano = require("gulp-cssnano");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var rename = require("gulp-rename");
var browserSync = require("browser-sync").create();
var concat = require("gulp-concat");
var replace = require("gulp-string-replace");
var plumber = require("gulp-plumber");
var del = require("del");
var runSequence = require("run-sequence");
var jsImport = require('gulp-js-import');
var minify = require('gulp-minify');
var swig = require("gulp-swig");
var include = require('gulp-include');
var replaceName = require('gulp-replace-name');
/**
 * @browserSync
 */
gulp.task("browserSync", function (done) {
    browserSync.init({
        injectChanges: true,
        server: {
            baseDir: "dist",
        },
    });

    done();
});

/**
 * @gulp-scss code
 */
gulp.task("scss:tools", function (done) {
    gulp
        .src("app/assets/scss/tools/index.scss")
        .pipe(
            plumber({
                errorHandler: function (error) {
                    console.log(error.toString());
                    this.emit("end");
                },
            })
        )
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat("index.scss"))
        .pipe(cssnano())
        .pipe(rename("tool.min.css"))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({ browsers: ["last 4 version"] }))
        .pipe(replace('../../../../dist/', '../'))
        .pipe(replace('../../../dist/', '../'))
        .pipe(replace('../../dist/', '../'))
        .pipe(gulp.dest("./dist/css/"))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        );

    done();
});
gulp.task("scss:pages", function (done) {
    gulp
        .src("app/assets/scss/pages/**/*.scss")
        .pipe(
            plumber({
                errorHandler: function (error) {
                    console.log(error.toString());
                    this.emit("end");
                },
            })
        )
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat("page.scss"))
        .pipe(cssnano())
        .pipe(rename("page.min.css"))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({ browsers: ["last 4 version"] }))
        .pipe(replace('../../../../dist/', '../'))
        .pipe(replace('../../../dist/', '../'))
        .pipe(replace('../../dist/', '../'))
        .pipe(gulp.dest("./dist/css/"))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        );

    done();
});
gulp.task("scss:mains", function (done) {
    gulp
        .src("app/assets/scss/mains/**/*.scss")
        .pipe(
            plumber({
                errorHandler: function (error) {
                    console.log(error.toString());
                    this.emit("end");
                },
            })
        )
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat("main.scss"))
        .pipe(cssnano())
        .pipe(rename("main.min.css"))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({ browsers: ["last 4 version"] }))
        .pipe(replace('../../../../dist/', '../'))
        .pipe(replace('../../../dist/', '../'))
        .pipe(replace('../../dist/', '../'))
        .pipe(gulp.dest("./dist/css/"))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        );

    done();
});
gulp.task("scss:responsive", function (done) {
    gulp
        .src("app/assets/scss/responsive/**/*.scss")
        .pipe(
            plumber({
                errorHandler: function (error) {
                    console.log(error.toString());
                    this.emit("end");
                },
            })
        )
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat("responsive.scss"))
        .pipe(cssnano())
        .pipe(rename("responsive.min.css"))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({ browsers: ["last 4 version"] }))
        .pipe(replace('../../../../dist/', '../'))
        .pipe(replace('../../../dist/', '../'))
        .pipe(replace('../../dist/', '../'))
        .pipe(gulp.dest("./dist/css/"))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        );

    done();
});
gulp.task("scss:upgrade", function (done) {
    gulp
        .src("app/assets/scss/upgrade/upgrade.scss")
        .pipe(
            plumber({
                errorHandler: function (error) {
                    console.log(error.toString());
                    this.emit("end");
                },
            })
        )
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat("upgrade.scss"))
        .pipe(cssnano())
        .pipe(rename("upgrade.min.css"))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({ browsers: ["last 4 version"] }))
        .pipe(replace('../../../../dist/', '../'))
        .pipe(replace('../../../dist/', '../'))
        .pipe(replace('../../dist/', '../'))
        .pipe(gulp.dest("./dist/css/"))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        );

    done();
});
/**
 * @gulp-import-js
 */
gulp.task('import:tool', function (done) {
    gulp.src('app/assets/js/tool.js')
        .pipe(jsImport({
            hideConsole: true
        }))
        .pipe(minify())
        .pipe(replaceName(/\-min.js/g, '.min.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({
            stream: true
        }));
    done();
});
gulp.task("import:main", function (done) {
    gulp
        .src("app/assets/js/main.js")
        .pipe(
            plumber({
                errorHandler: function (error) {
                    console.log(error.toString());
                    this.emit("end");
                },
            })
        )
        .pipe(gulp.dest("dist/js/"))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        );
    done();
});
/**
 * @gulp-swig code html
 */
gulp.task("views", function (done) {
    gulp.src("app/views/*.swig")

        .pipe(
            plumber({
                errorHandler: function (error) {
                    console.log(error.toString());
                    this.emit("end");
                },
            })
        )
        .pipe(include())
        .pipe(
            swig({
                defaults: {
                    cache: false,
                },
            })
        )
        .pipe(replace('../../../../dist/', ''))
        .pipe(replace('../../../dist/', ''))
        .pipe(replace('../../dist/', ''))
        .pipe(gulp.dest("dist/"))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        );

    done();
});
/**
 * @fonts
 */
gulp.task("fonts", function (done) {
    gulp.src("app/assets/fonts/**/*").pipe(gulp.dest("dist/fonts"));

    done();
});
/**
 * @watch-call
 */
gulp.task("watch", function (done) {
    gulp.watch("app/assets/scss/tools/index.scss", ["scss-tools:watch"]);
    gulp.watch("app/assets/scss/pages/**/*", ["scss-pages:watch"]);
    gulp.watch("app/assets/scss/mains/**/*", ["scss-mains:watch"]);
    gulp.watch("app/assets/scss/responsive/**/*", ["scss-responsive:watch"]);
    gulp.watch("app/assets/scss/upgrade/**/*", ["scss-upgrade:watch"]);
    gulp.watch('app/assets/js/tools/**/*', ['import-tool:watch']);
    gulp.watch("app/assets/js/tool.js", ["import-tool:watch"]);
    gulp.watch("app/assets/js/main.js", ["import-main:watch"]);
    gulp.watch("app/views/**/*", ["views:watch"]);
    gulp.watch("app/assets/**/*", function () {
        browserSync.reload({
            stream: true,
        });
    });

    done();
});

/**
 * @callback
 */
gulp.task("import-tool:watch", function (callback) {
    runSequence("import:tool", callback);
});
gulp.task("import-main:watch", function (callback) {
    runSequence("import:main", callback);
});

gulp.task("views:watch", function (callback) {
    runSequence("views", callback);
});
gulp.task("scss-pages:watch", function (callback) {
    runSequence("scss:pages", callback);
});
gulp.task("scss-mains:watch", function (callback) {
    runSequence("scss:mains", callback);
});
gulp.task("scss-tools:watch", function (callback) {
    runSequence("scss:tools", callback);
});
gulp.task("scss-responsive:watch", function (callback) {
    runSequence("scss:responsive", callback);
});
gulp.task("scss-upgrade:watch", function (callback) {
    runSequence("scss:upgrade", callback);
});
/**
 * @clean
 */
gulp.task("clean:js", function (done) {
    del.sync(['dist/js/main.js', 'dist/js/tool.js']);
    done();
});
gulp.task("clean:css", function (done) {
    del.sync(["dist/css"]);
    done();
});

gulp.task("clean:images", function (done) {
    del.sync("dist/images");
    done();

});
gulp.task("clean:dist", function (done) {
    del.sync([
        "dist/images",
        "dist/fonts",
        "dist/js",
        "dist/css",
        "dist/**/*.html",
    ]);
    done();

});
gulp.task("clean:html", function (done) {
    del.sync("dist/**/*.html");
    done();

});
/**
 * @build
 */

gulp.task("default", function (callback) {
    runSequence(
        "clean:html",
        [
            "views",
            "scss:pages",
            "scss:upgrade",
            "scss:mains",
            "scss:tools",
            "scss:responsive",
            "fonts",
            "import:tool",
            "import:main",
        ],
        "browserSync",
        ['watch', 'clean:js'],
        callback
    );
});