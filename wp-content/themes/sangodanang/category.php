<?php get_header(); ?>

<?php
	$category_info = get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id = $category_info->term_id;
	$cat_name = get_cat_name($cat_id);
?>

<main id="main">
        
	<article class="section section-breadcrumbs">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__breadcrumbs">
	                    <div class="module__header">
	                        <h1 class="title"><?php echo $cat_name; ?></h1>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>

	<article class="section section-blogs">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module__blogs module__blogs__list">
	                    <div class="module__content">

	                        <div class="groups_box">
								<?php
									$query = query_post_by_category_paged($cat_id, 6);
									if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>

								    <div class="item">
								        <div class="module_item blog_item">
								            <div class="item_contents">
								                <h4 class="item_name">
								                    <a href="<?php the_permalink();?>">
								                        <?php the_title(); ?>
								                    </a>
								                </h4>
								                <div class="item_create">
								                    <label><?php echo get_the_date('d');?></label>
								                    <span><?php _e('Tháng', 'text_domain'); ?> <?php echo get_the_date('m/Y');?></span>
								                </div>
								            </div>
								            <div class="item_images">
								                <div class="image">
								                    <a href="<?php the_permalink();?>">
								                        <img src="<?php echo getPostImage(get_the_ID(),"p-news"); ?>" alt="<?php the_title(); ?>">
								                    </a>
								                </div>
								            </div>
								        </div>
								    </div>

								<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>
	                        </div>
							
							<nav class="navigation">
								<?php wp_pagenavi( array( 'query' => $query ) ); ?>
							</nav>

	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>

</main>

<?php get_footer(); ?>