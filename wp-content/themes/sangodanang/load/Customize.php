<?php
function customizer_data() {
	$customizer_data = [];
	$customizer_data = [
		[
			'info' => [
				'name' => 'header',
				'label' => 'Header',
				'description' => 'Section header',
				'priority' => 1,
			],
			'fields' => [
				[
					'name' => 'header_logo',
					'type' => 'image',
					'default' => '',
					'label' => 'Logo',
				],
				[
					'name' => 'header_phone',
					'type' => 'text',
					'default' => '',
					'label' => 'Hotline',
				],
				[
					'name' => 'header_gmail',
					'type' => 'text',
					'default' => '',
					'label' => 'Link gmail',
				],
				[
					'name' => 'header_facebook',
					'type' => 'text',
					'default' => '',
					'label' => 'Link facebook',
				],
			],
		],
		[
			'info' => [
				'name' => 'footer',
				'label' => 'Footer',
				'description' => 'Section footer',
				'priority' => 2,
			],
			'fields' => [
				[
					'name' => 'footer_logo',
					'type' => 'image',
					'default' => '',
					'label' => 'Logo',
				],
				[
					'name' => 'footer_info',
					'type' => 'textarea',
					'default' => '',
					'label' => 'Thông tin',
				],
				[
					'name' => 'footer_info_en',
					'type' => 'textarea',
					'default' => '',
					'label' => 'Thông tin bên tiếng anh',
				],
				[
					'name' => 'footer_form',
					'type' => 'text',
					'default' => '',
					'label' => 'ShortCode Form',
				],
				[
					'name' => 'footer_form_en',
					'type' => 'text',
					'default' => '',
					'label' => 'ShortCode Form bên tiếng anh',
				],
			],
		],		
	];
	return $customizer_data;
}


function customizer_customizer( $wp_customize ) {
	$customizer_data =  customizer_data();

	foreach ($customizer_data as $customizer_section) {

		$ctm_section_name = $customizer_section["info"]["name"];
		$ctm_section_label = $customizer_section["info"]["label"];
		$ctm_section_description = $customizer_section["info"]["description"];
		$ctm_section_priority = $customizer_section["info"]["priority"];

	    $wp_customize->add_section (
	        $ctm_section_name,
	        array(
	            'title' => $ctm_section_label,
	            'description' => $ctm_section_description,
	            'priority' => $ctm_section_priority,
	        )
	    );

		foreach ($customizer_section["fields"] as $customizer_fields) {

			$ctm_name = $customizer_fields["name"];
			$ctm_type = $customizer_fields["type"];
			$ctm_default = $customizer_fields["default"];
			$ctm_label = $customizer_fields["label"];
			if(isset($customizer_fields["choices"])) {
				$ctm_choices = $customizer_fields["choices"];
			}

			if ($ctm_type == 'text') {
			    $wp_customize->add_setting (
			    	$ctm_name,
	                array(
		                'default' => $ctm_default,
		            )
            	);
			    $wp_customize->add_control (
			        $ctm_name,
			        array(
			            'type' => $ctm_type,
			            'label' => $ctm_label,
			            'section' => $ctm_section_name,
			            'settings' => $ctm_name,
			        )
			    );
			} elseif($ctm_type == 'select') {
			    $wp_customize->add_setting( $ctm_name );
			    $wp_customize->add_control(
			        $ctm_name,
			        array(
			            'type' => $ctm_type,
			            'label' => $ctm_label,
			            'section' => $ctm_section_name,
			            'settings' => $ctm_name,
			            
			            'choices' => $ctm_choices,
			        )
			    );
			} elseif($ctm_type == 'radio') {
			    $wp_customize->add_setting(
			        $ctm_name,
			        array(
			            'default' => $ctm_default,
			        )
			    );
			    $wp_customize->add_control(
			        $ctm_name,
			        array(
			            'type' => $ctm_type,
			            'label' => $ctm_label,
			            'section' => $ctm_section_name,
			            'settings' => $ctm_name,

			            'choices' => $ctm_choices,
			        )
			    );
			} elseif($ctm_type == 'checkbox') {
			    $wp_customize->add_setting ( $ctm_name );
			    $wp_customize->add_control(
			        $ctm_name,
			        array(
			            'type' => $ctm_type,
			            'label' => $ctm_label,
			            'section' => $ctm_section_name,
			            'settings' => $ctm_name,
			        )
			    );
			} elseif($ctm_type == 'image') {
			    $wp_customize->add_setting(
			    	$ctm_name,
	                array(
		                'default' => $ctm_default,
		            )
	            );
			    $wp_customize->add_control(
			        new WP_Customize_Image_Control(
			            $wp_customize,
			            $ctm_name,
			            array(
			                'type' => $ctm_type,
			                'label' => $ctm_label,
			                'section' => $ctm_section_name,
			                'settings' => $ctm_name,
			            )
			        )
			    );
			} elseif($ctm_type == 'color') {
			    $wp_customize->add_setting (
			    	$ctm_name,
	                array(
		                'default' => $ctm_default,
		            )
            	);
			    $wp_customize->add_control(
			        new WP_Customize_Color_Control(
			            $wp_customize,
			            $ctm_name,
			            array(
			                'label' => $ctm_label,
			                'section' => $ctm_section_name,
			                'settings' => $ctm_name,
			            )
			        )
			    );
			} elseif($ctm_type == 'textarea') {
			    $wp_customize->add_setting (
			    	$ctm_name,
	                array(
		                'default' => $ctm_default,
		            )
            	);
			    $wp_customize->add_control (
			        $ctm_name,
			        array(
			            'type' => $ctm_type,
			            'label' => $ctm_label,
			            'section' => $ctm_section_name,
			            'settings' => $ctm_name,
			        )
			    );
			}
		}
	}
}
	
add_action( 'customize_register', 'customizer_customizer' );
?>