<?php get_header(); ?>

<main id="main">
    <div class="page__cart">
        <div class="module module__cart">
            <div class="module__header">
                <h2 class="title">
                    <?php
                        // the_title();
                    ?>
                    <?php
                        if(is_page( 324 )) {
                            _e('Thông báo', 'text_domain');
                        } else {
                            the_title();
                        }
                    ?>
                </h2>
            </div>
            <div class="module__content">
                <div class="container">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>